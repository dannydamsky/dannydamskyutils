package com.damsky.danny.dannydamskyutils

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import kotlinx.android.synthetic.main.activity_about_page.*

/**
 * About page template, ideal for open source projects.
 *
 * @author Danny Damsky
 * @since 2018-03-01
 */

abstract class AboutPage : AppCompatActivity() {

    private fun Button.setImage(imageResource: Int) {
        this.setCompoundDrawablesRelativeWithIntrinsicBounds(imageResource, 0, 0, 0)
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {

        /**
         * Sets the theme to the style given by the abstract function.
         */
        setTheme(setActivityTheme())

        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_about_page)

        /**
         * Set image and text for mainButton from the abstract functions.
         */
        mainButton.setImage(setAppIcon())
        mainButton.text = setAppName()

        /**
         * Set image and text for versionButton from the abstract functions.
         */
        versionButton.setImage(setVersionIcon())
        versionButton.text = "${versionButton.text} ${setVersion()}"

        /**
         * Set image for introButton from the abstract function.
         */
        introButton.setImage(setIntroIcon())

        /**
         * Set image for sourceCodeButton from the abstract function.
         */
        sourceCodeButton.setImage(setSourceCodeIcon())

        /**
         * Set image for authorButton from the abstract function.
         */
        authorButton.setImage(setAuthorTitleIcon())

        /**
         * Set image and text for authorNameButton from the abstract functions.
         */
        authorNameButton.setImage(setAuthorNameIcon())
        authorNameButton.text = setAuthorName()

        /**
         * Set image and text for authorLocationButton from the abstract functions.
         */
        authorLocationButton.setImage(setAuthorLocationIcon())
        authorLocationButton.text = setAuthorLocation()

        /**
         * Set image for emailButton from the abstract function.
         */
        emailButton.setImage(setEmailIcon())

        /**
         * Set image for supportButton from the abstract function.
         */
        supportButton.setImage(setSupportIcon())

        /**
         * Set image for rateButton from the abstract function.
         */
        rateButton.setImage(setRateIcon())

        /**
         * Set image for donateButton from the abstract function.
         */
        donateButton.setImage(setDonateIcon())


        /**
         * Set onClickListeners to all the useful buttons.
         */
        introButton.setOnClickListener { openIntro() }
        sourceCodeButton.setOnClickListener { openSourceCode() }
        emailButton.setOnClickListener { writeEmail() }
        rateButton.setOnClickListener { openStorePageRating() }
        donateButton.setOnClickListener { openStoreDonationPage() }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    /**
     * Sets the back arrow button on the toolbar.
     */
    protected fun setBackButton(boolean: Boolean) {
        supportActionBar?.setDisplayHomeAsUpEnabled(boolean)
        supportActionBar?.setDisplayShowHomeEnabled(boolean)
    }

    /**
     * Sets the theme for the activity.
     */
    abstract fun setActivityTheme(): Int

    /**
     * Set the app icon.
     */
    abstract fun setAppIcon(): Int

    /**
     * Set the app name.
     */
    abstract fun setAppName(): String

    /**
     * Set the icon for the version button.
     */
    abstract fun setVersionIcon(): Int

    /**
     * Return your application's version, it will appear on the version button.
     */
    abstract fun setVersion(): String

    /**
     * Set the icon for the intro button.
     */
    abstract fun setIntroIcon(): Int

    /**
     * Set the icon for the source code button.
     */
    abstract fun setSourceCodeIcon(): Int

    /**
     * Set the icon for the Author title button.
     */
    abstract fun setAuthorTitleIcon(): Int

    /**
     * Set the name of the author.
     */
    abstract fun setAuthorName(): String

    /**
     * Set the icon for the icon with the author's name.
     */
    abstract fun setAuthorNameIcon(): Int

    /**
     * Set the location for the author.
     */
    abstract fun setAuthorLocation(): String

    /**
     * Set the icon for the author's location button.
     */
    abstract fun setAuthorLocationIcon(): Int

    /**
     * Set the icon for the email button.
     */
    abstract fun setEmailIcon(): Int

    /**
     * Set the icon for the Support title button.
     */
    abstract fun setSupportIcon(): Int

    /**
     * Set the icon for the Rate button.
     */
    abstract fun setRateIcon(): Int

    /**
     * Set the icon for the Donate button
     */
    abstract fun setDonateIcon(): Int

    /**
     * This action will launch when the Introduction button is pressed
     */
    abstract fun openIntro()

    /**
     * This action will launch when the Source Code button is pressed
     */
    abstract fun openSourceCode()

    /**
     * This action will launch when the Write Email button is pressed
     */
    abstract fun writeEmail()

    /**
     * This action will launch when the Rate button is pressed
     */
    abstract fun openStorePageRating()

    /**
     * This action will launch when the Donate button is pressed
     */
    abstract fun openStoreDonationPage()
}
