package com.damsky.danny.dannydamskyutils

import android.app.Activity
import android.app.TimePickerDialog
import android.content.DialogInterface
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.text.InputType
import android.view.View
import android.view.WindowManager
import android.widget.*

/**
 * This class is used for displaying elements on screen that aren't part of the layout.
 * Such elements include: Toasts, Snackbars, AlertDialogs and TimePickerDialogs.
 *
 * @see Toast
 * @see Snackbar
 * @see AlertDialog
 * @see TimePickerDialog
 *
 * @author Danny Damsky
 * @since 2018-03-01
 */

class Display {
    private val context: Activity
    private var iconResource = -1
    private val view: View

    /**
     * @constructor        Caches the default Android content view for use with Snackbar.
     * @param context      Binds object to activity.
     * @param iconResource A resource drawable/mipmap to be used for alert dialogs.
     *
     */
    constructor(context: Activity, iconResource: Int) {
        this.context = context
        this.iconResource = iconResource
        this.view = this.context.findViewById(android.R.id.content)
    }

    /**
     * @param context      Binds object to activity.
     * @param iconResource A resource drawable/mipmap to be used for alert dialogs.
     * @param view         Caches in a view object for future use with Snackbar.
     */
    constructor(context: Activity, iconResource: Int, view: View) {
        this.context = context
        this.iconResource = iconResource
        this.view = view
    }


    /**
     * Displays a short toast message.
     *
     * @param resourceId A string resource file for the Toast to display.
     * @see Toast
     */
    fun showToastShort(resourceId: Int) {
        Toast.makeText(context.applicationContext, resourceId, Toast.LENGTH_SHORT).show()
    }

    /**
     * Displays a short toast message.
     *
     * @param text A string for the Toast to display.
     * @see Toast
     */
    fun showToastShort(text: String) {
        Toast.makeText(context.applicationContext, text, Toast.LENGTH_SHORT).show()
    }

    /**
     * Displays a long toast message.
     *
     * @param resourceId A string resource file for the Toast to display.
     * @see Toast
     */
    fun showToastLong(resourceId: Int) {
        Toast.makeText(context.applicationContext, resourceId, Toast.LENGTH_LONG).show()
    }

    /**
     * Displays a long toast message.
     *
     * @param text A string for the Toast to display.
     * @see Toast
     */
    fun showToastLong(text: String) {
        Toast.makeText(context.applicationContext, text, Toast.LENGTH_LONG).show()
    }

    /**
     * Displays a short Snackbar message.
     *
     * @param resourceId A resource drawable/mipmap for the Snackbar to display.
     * @see Snackbar
     */
    fun showSnackShort(resourceId: Int) {
        Snackbar.make(view, resourceId, Snackbar.LENGTH_SHORT).show()
    }

    /**
     * Displays a short Snackbar message.
     *
     * @param text A string for the Snackbar to display.
     * @see Snackbar
     */
    fun showSnackShort(text: String) {
        Snackbar.make(view, text, Snackbar.LENGTH_SHORT).show()
    }

    /**
     * Displays a long Snackbar message.
     *
     * @param resourceId A resource drawable/mipmap for the Snackbar to display.
     * @see Snackbar
     */
    fun showSnackLong(resourceId: Int) {
        Snackbar.make(view, resourceId, Snackbar.LENGTH_LONG).show()
    }

    /**
     * Displays a long Snackbar message.
     *
     * @param text A string for the Snackbar to display.
     * @see Snackbar
     */
    fun showSnackLong(text: String) {
        Snackbar.make(view, text, Snackbar.LENGTH_LONG).show()
    }

    /**
     * Displays a Snackbar message until user manually swipes it off the screen.
     *
     * @param resourceId A resource drawable/mipmap for the Snackbar to display.
     * @see Snackbar
     */
    fun showSnackIndefinite(resourceId: Int) {
        Snackbar.make(view, resourceId, Snackbar.LENGTH_INDEFINITE).show()
    }

    /**
     * Displays a Snackbar message until user manually swipes it off the screen.
     *
     * @param text A string for the Snackbar to display.
     * @see Snackbar
     */
    fun showSnackIndefinite(text: String) {
        Snackbar.make(view, text, Snackbar.LENGTH_INDEFINITE).show()
    }


    /**
     * A basic dialog popup with a title, a message (A question to ask the user) and yes/no options
     * for the user to answer.
     *
     * @param title          The dialog's title.
     * @param message        A message to display to the user.
     * @param positiveAction Higher-order-function for running code on positive button press.
     * @param negativeAction Higher-order-function for running code on negative button press.
     *
     * @see AlertDialog
     */
    fun showBasicDialog(title: Int, message: Int, positiveAction: () -> Unit, negativeAction: () -> Unit = {}) {
        showBasicDialog(getDialogBuilder(title, message), positiveAction, negativeAction)
    }

    /**
     * A basic dialog popup with a title, a message (A question to ask the user) and yes/no options
     * for the user to answer.
     *
     * @param title          The dialog's title.
     * @param message        A message to display to the user.
     * @param positiveAction Higher-order-function for running code on positive button press.
     * @param negativeAction Higher-order-function for running code on negative button press.
     *
     * @see AlertDialog
     */
    fun showBasicDialog(title: String, message: Int, positiveAction: () -> Unit, negativeAction: () -> Unit = {}) {
        showBasicDialog(getDialogBuilder(title, message), positiveAction, negativeAction)
    }

    /**
     * A basic dialog popup with a title, a message (A question to ask the user) and yes/no options
     * for the user to answer.
     *
     * @param title          The dialog's title.
     * @param message        A message to display to the user.
     * @param positiveAction Higher-order-function for running code on positive button press.
     * @param negativeAction Higher-order-function for running code on negative button press.
     *
     * @see AlertDialog
     */
    fun showBasicDialog(title: Int, message: String, positiveAction: () -> Unit, negativeAction: () -> Unit = {}) {
        showBasicDialog(getDialogBuilder(title, message), positiveAction, negativeAction)
    }

    /**
     * A basic dialog popup with a title, a message (A question to ask the user) and yes/no options
     * for the user to answer.
     *
     * @param title          The dialog's title.
     * @param message        A message to display to the user.
     * @param positiveAction Higher-order-function for running code on positive button press.
     * @param negativeAction Higher-order-function for running code on negative button press.
     *
     * @see AlertDialog
     */
    fun showBasicDialog(title: String, message: String, positiveAction: () -> Unit, negativeAction: () -> Unit = {}) {
        showBasicDialog(getDialogBuilder(title, message), positiveAction, negativeAction)
    }

    /**
     * A dialog popup with a title, a hint (Text that will appear on the editText while it's empty),
     * an editText object to allow the user to insert a string and ok/cancel options for the user
     * to confirm the action.
     *
     * @param title          The dialog's title.
     * @param hint           A hint to display in the EditText while it's empty.
     * @param editText       The EditText that shall be modified and set to the AlertDialog.
     * @param positiveAction Higher-order-function for running things on positive button press.
     * @param negativeAction Higher-order-function for running code on negative button press.
     *
     * @see AlertDialog
     * @see EditText
     */
    fun showEditTextDialog(title: Int, hint: Int, editText: EditText, positiveAction: () -> Unit, negativeAction: () -> Unit = {}) {
        editText.setHint(hint)
        showDialogWithEditText(getDialogBuilder(title), editText, positiveAction, negativeAction)
    }

    /**
     * A dialog popup with a title, a hint (Text that will appear on the editText while it's empty),
     * an editText object to allow the user to insert a string and ok/cancel options for the user
     * to confirm the action.
     *
     * @param title          The dialog's title.
     * @param hint           A hint to display in the EditText while it's empty.
     * @param editText       The EditText that shall be modified and set to the AlertDialog.
     * @param positiveAction Higher-order-function for running things on positive button press.
     * @param negativeAction Higher-order-function for running code on negative button press.
     *
     * @see AlertDialog
     * @see EditText
     */
    fun showEditTextDialog(title: Int, hint: String, editText: EditText, positiveAction: () -> Unit, negativeAction: () -> Unit = {}) {
        editText.hint = hint
        showDialogWithEditText(getDialogBuilder(title), editText, positiveAction, negativeAction)
    }

    /**
     * A dialog popup with a title, a hint (Text that will appear on the editText while it's empty),
     * an editText object to allow the user to insert a string and ok/cancel options for the user
     * to confirm the action.
     *
     * @param title          The dialog's title.
     * @param hint           A hint to display in the EditText while it's empty.
     * @param editText       The EditText that shall be modified and set to the AlertDialog.
     * @param positiveAction Higher-order-function for running things on positive button press.
     * @param negativeAction Higher-order-function for running code on negative button press.
     *
     * @see AlertDialog
     * @see EditText
     */
    fun showEditTextDialog(title: String, hint: Int, editText: EditText, positiveAction: () -> Unit, negativeAction: () -> Unit = {}) {
        editText.setHint(hint)
        showDialogWithEditText(getDialogBuilder(title), editText, positiveAction, negativeAction)
    }

    /**
     * A dialog popup with a title, a hint (Text that will appear on the editText while it's empty),
     * an editText object to allow the user to insert a string and ok/cancel options for the user
     * to confirm the action.
     *
     * @param title          The dialog's title.
     * @param hint           A hint to display in the EditText while it's empty.
     * @param editText       The EditText that shall be modified and set to the AlertDialog.
     * @param positiveAction Higher-order-function for running things on positive button press.
     * @param negativeAction Higher-order-function for running code on negative button press.
     *
     * @see AlertDialog
     * @see EditText
     */
    fun showEditTextDialog(title: String, hint: String, editText: EditText, positiveAction: () -> Unit, negativeAction: () -> Unit = {}) {
        editText.hint = hint
        showDialogWithEditText(getDialogBuilder(title), editText, positiveAction, negativeAction)
    }

    /**
     * A dialog popup with a title, a list of items which the
     * user can choose (multiple choices are allowed) from, and boolean list with the same size as the
     * item list (used for keeping true/false values according to wether the item was checked/unchecked
     * by the user or manually by code).
     *
     *
     * @param title          The dialog's title.
     * @param itemList       A string array of item names to apply to the multi choice.
     * @param boolList       A boolean array the same size as itemList.
     * @param positiveAction Higher-order-function for running things on positive button press.
     * @param negativeAction Higher-order-function for running code on negative button press.
     *
     * @see AlertDialog
     * @throws IllegalStateException If boolList isn't the same size as itemList
     */
    fun showMultiChoiceDialog(title: Int, itemList: Array<String>, boolList: BooleanArray, positiveAction: () -> Unit, negativeAction: () -> Unit = {}) {
        showMultiChoiceDialog(getDialogBuilder(title), itemList, boolList, positiveAction, negativeAction)
    }

    /**
     * A dialog popup with a title, a list of items which the
     * user can choose (multiple choices are allowed) from, and boolean list with the same size as the
     * item list (used for keeping true/false values according to wether the item was checked/unchecked
     * by the user or manually by code).
     *
     *
     * @param title          The dialog's title.
     * @param itemList       A string array of item names to apply to the multi choice.
     * @param boolList       A boolean array the same size as itemList.
     * @param positiveAction Higher-order-function for running things on positive button press.
     * @param negativeAction Higher-order-function for running code on negative button press.
     *
     * @see AlertDialog
     * @throws IllegalStateException If boolList isn't the same size as itemList
     */
    fun showMultiChoiceDialog(title: String, itemList: Array<String>, boolList: BooleanArray, positiveAction: () -> Unit, negativeAction: () -> Unit = {}) {
        showMultiChoiceDialog(getDialogBuilder(title), itemList, boolList, positiveAction, negativeAction)
    }

    /**
     * A dialog popup with a title, a message, a spinner object which contains a string list
     * as options to choose from. Confirmation is done by ok/cancel buttons.
     *
     * @param title          The dialog's title.
     * @param message        A message to display to the user.
     * @param spinner        A spinner object to show in the AlertDialog.
     * @param list           A list of items to display in the spinner.
     * @param positiveAction Higher-order-function for running things on positive button press.
     * @param negativeAction Higher-order-function for running code on negative button press.
     *
     * @see AlertDialog
     * @see Spinner
     */
    fun showSpinnerDialog(title: Int, message: Int, spinner: Spinner, list: List<String>, positiveAction: () -> Unit, negativeAction: () -> Unit = {}) {
        showSpinnerDialog(getDialogBuilder(title, message), spinner, list, positiveAction, negativeAction)
    }

    /**
     * A dialog popup with a title, a message, a spinner object which contains a string list
     * as options to choose from. Confirmation is done by ok/cancel buttons.
     *
     * @param title          The dialog's title.
     * @param message        A message to display to the user.
     * @param spinner        A spinner object to show in the AlertDialog.
     * @param list           A list of items to display in the spinner.
     * @param positiveAction Higher-order-function for running things on positive button press.
     * @param negativeAction Higher-order-function for running code on negative button press.
     *
     * @see AlertDialog
     * @see Spinner
     */
    fun showSpinnerDialog(title: String, message: Int, spinner: Spinner, list: List<String>, positiveAction: () -> Unit, negativeAction: () -> Unit = {}) {
        showSpinnerDialog(getDialogBuilder(title, message), spinner, list, positiveAction, negativeAction)
    }

    /**
     * A dialog popup with a title, a message, a spinner object which contains a string list
     * as options to choose from. Confirmation is done by ok/cancel buttons.
     *
     * @param title          The dialog's title.
     * @param message        A message to display to the user.
     * @param spinner        A spinner object to show in the AlertDialog.
     * @param list           A list of items to display in the spinner.
     * @param positiveAction Higher-order-function for running things on positive button press.
     * @param negativeAction Higher-order-function for running code on negative button press.
     *
     * @see AlertDialog
     * @see Spinner
     */
    fun showSpinnerDialog(title: Int, message: String, spinner: Spinner, list: List<String>, positiveAction: () -> Unit, negativeAction: () -> Unit = {}) {
        showSpinnerDialog(getDialogBuilder(title, message), spinner, list, positiveAction, negativeAction)
    }

    /**
     * A dialog popup with a title, a message, a spinner object which contains a string list
     * as options to choose from. Confirmation is done by ok/cancel buttons.
     *
     * @param title          The dialog's title.
     * @param message        A message to display to the user.
     * @param spinner        A spinner object to show in the AlertDialog.
     * @param list           A list of items to display in the spinner.
     * @param positiveAction Higher-order-function for running things on positive button press.
     * @param negativeAction Higher-order-function for running code on negative button press.
     *
     * @see AlertDialog
     * @see Spinner
     */
    fun showSpinnerDialog(title: String, message: String, spinner: Spinner, list: List<String>, positiveAction: () -> Unit, negativeAction: () -> Unit = {}) {
        showSpinnerDialog(getDialogBuilder(title, message), spinner, list, positiveAction, negativeAction)
    }

    /**
     * A dialog popup with a title and a list of items to display as single-choice options.
     * Confirmation is done by ok/cancel buttons.
     *
     * @param title          The dialog's title.
     * @param items          List of items to show in the AlertDialog.
     * @param positiveAction Higher-order-function for running things on positive button press.
     *
     * @see AlertDialog
     */
    fun showListDialog(title: Int, items: Array<String>, positiveAction: (which: Int) -> Unit) {
        showListDialog(getDialogBuilder(title), items, positiveAction)
    }

    /**
     * A dialog popup with a title and a list of items to display as single-choice options.
     * Confirmation is done by ok/cancel buttons.
     *
     * @param title          The dialog's title.
     * @param items          List of items to show in the AlertDialog.
     * @param positiveAction Higher-order-function for running things on positive button press.
     *
     * @see AlertDialog
     */
    fun showListDialog(title: String, items: Array<String>, positiveAction: (which: Int) -> Unit) {
        showListDialog(getDialogBuilder(title), items, positiveAction)
    }

    /**
     * A Time Picker dialog popup, requires the user to enter the time in hours/minutes and
     * afterwards press ok or cancel if he regrets using the function.
     *
     * @param title          The dialog's title.
     * @param startHour      Default hour to be set in the TimePickerDialog.
     * @param startMinute    Default minute to be set in the TimePickerDialog.
     * @param positiveAction Higher-order-function for running things on positive button press.
     *
     * @see TimePickerDialog
     */
    fun showTimeDialog(title: Int, startHour: Int, startMinute: Int, positiveAction: (hourOfDay: Int, minute: Int) -> Unit) {
        val timePickerDialog = getTimePickerDialog(startHour, startMinute, positiveAction)
        timePickerDialog.setTitle(title)
        timePickerDialog.show()
    }

    /**
     * A Time Picker dialog popup, requires the user to enter the time in hours/minutes and
     * afterwards press ok or cancel if he regrets using the function.
     *
     * @param title          The dialog's title.
     * @param startHour      Default hour to be set in the TimePickerDialog.
     * @param startMinute    Default minute to be set in the TimePickerDialog.
     * @param positiveAction Higher-order-function for running things on positive button press.
     *
     * @see TimePickerDialog
     */
    fun showTimeDialog(title: String, startHour: Int, startMinute: Int, positiveAction: (hourOfDay: Int, minute: Int) -> Unit) {
        val timePickerDialog = getTimePickerDialog(startHour, startMinute, positiveAction)
        timePickerDialog.setTitle(title)
        timePickerDialog.show()
    }


    // Private build helper functions

    private fun getDialogBuilder(): AlertDialog.Builder {
        return AlertDialog.Builder(context).setIcon(iconResource)
    }

    private fun getDialogBuilder(title: Int): AlertDialog.Builder {
        return getDialogBuilder().setTitle(title)
    }

    private fun getDialogBuilder(title: String): AlertDialog.Builder {
        return getDialogBuilder().setTitle(title)
    }

    private fun getDialogBuilder(title: Int, message: Int): AlertDialog.Builder {
        return getDialogBuilder(title).setMessage(message)
    }

    private fun getDialogBuilder(title: String, message: Int): AlertDialog.Builder {
        return getDialogBuilder(title).setMessage(message)
    }

    private fun getDialogBuilder(title: Int, message: String): AlertDialog.Builder {
        return getDialogBuilder(title).setMessage(message)
    }

    private fun getDialogBuilder(title: String, message: String): AlertDialog.Builder {
        return getDialogBuilder(title).setMessage(message)
    }

    private fun setDialogActions(builder: AlertDialog.Builder, positiveText: Int, negativeText: Int, positiveAction: () -> Unit, negativeAction: () -> Unit) {
        builder.setPositiveButton(positiveText, { dialog, _ ->
            positiveAction()
            dialog.dismiss()
        })

        builder.setNegativeButton(negativeText, { dialog, _ ->
            negativeAction()
            dialog.dismiss()
        })
    }

    private fun showBasicDialog(builder: AlertDialog.Builder, positiveAction: () -> Unit, negativeAction: () -> Unit) {
        setDialogActions(builder, android.R.string.yes, android.R.string.no, positiveAction, negativeAction)
        builder.show()
    }

    private fun showDialogWithEditText(builder: AlertDialog.Builder, editText: EditText, positiveAction: () -> Unit, negativeAction: () -> Unit) {
        editText.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_SENTENCES
        editText.setSelection(editText.text.length)

        builder.setView(editText)

        setDialogActions(builder, android.R.string.ok, android.R.string.cancel, positiveAction, negativeAction)

        val dialog = builder.create()
        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        dialog.show()
    }

    private fun showMultiChoiceDialog(builder: AlertDialog.Builder, itemList: Array<String>, boolList: BooleanArray, positiveAction: () -> Unit, negativeAction: () -> Unit) {
        if (boolList.size == itemList.size) {
            builder.setMultiChoiceItems(itemList, boolList, { _: DialogInterface, which: Int, isChecked: Boolean ->
                boolList[which] = isChecked
            })

            setDialogActions(builder, android.R.string.ok, android.R.string.cancel, positiveAction, negativeAction)

            builder.show()
        } else
            throw IllegalStateException("booleanArray must be the same size as itemList")
    }

    private fun showSpinnerDialog(builder: AlertDialog.Builder, spinner: Spinner, arrayList: List<String>, positiveAction: () -> Unit, negativeAction: () -> Unit) {
        spinner.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT)
        spinner.adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, arrayList)
        builder.setView(spinner)

        setDialogActions(builder, android.R.string.ok, android.R.string.cancel, positiveAction, negativeAction)

        builder.show()
    }

    private fun showListDialog(builder: AlertDialog.Builder, items: Array<String>, positiveAction: (which: Int) -> Unit) {
        builder.setItems(items) { _, which: Int -> positiveAction(which) }
        builder.show()
    }

    private fun getTimePickerDialog(startHour: Int, startMinute: Int, positiveAction: (hourOfDay: Int, minute: Int) -> Unit): TimePickerDialog {
        return TimePickerDialog(context, TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
            positiveAction(hourOfDay, minute)
        }, startHour, startMinute, true)
    }
}
