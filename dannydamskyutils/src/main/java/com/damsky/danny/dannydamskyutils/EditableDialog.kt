package com.damsky.danny.dannydamskyutils

import android.app.Dialog
import android.content.Context
import android.graphics.drawable.Drawable
import android.view.View
import android.view.WindowManager
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.editable_dialog.*

/**
 * Custom dialog with a AutoCompleteTextView, a Seekbar and ok/cancel buttons.
 *
 * @param context               Required parameter for any dialog.
 * @param autoCompleteList      A list of items that will be used as suggestions for the Editable.
 *
 * @author Danny Damsky
 * @since 2018-03-13
 */
class EditableDialog private constructor(context: Context, autoCompleteList: ArrayList<String>) : Dialog(context) {
    init {
        setContentView(R.layout.editable_dialog)

        val adapter = ArrayAdapter<String>(context, android.R.layout.select_dialog_item, autoCompleteList)
        editableDialogEditableView.setAdapter(adapter)

        editableDialogEditableView.requestFocus()
        setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
    }

    fun setInputType(type: Int) {
        editableDialogEditableView.inputType = type
    }

    fun setSoftInputMode(mode: Int) {
        window.setSoftInputMode(mode)
    }

    class Builder(context: Context, autoCompleteList: ArrayList<String>) {
        private val dialog: EditableDialog = EditableDialog(context, autoCompleteList)

        private fun setDefaultListenersIfNotExist() {
            if (!dialog.editableDialogOk.hasOnClickListeners())
                dialog.editableDialogOk.setOnClickListener { dialog.dismiss() }

            if (!dialog.editableDialogCancel.hasOnClickListeners())
                dialog.editableDialogCancel.setOnClickListener { dialog.dismiss() }
        }

        fun create(): EditableDialog {
            setDefaultListenersIfNotExist()
            return dialog
        }

        fun show() {
            setDefaultListenersIfNotExist()
            dialog.show()
        }

        /**
         * Set the icon of the dialog to a drawable resource.
         *
         * @param resourceId Drawable resource id.
         */
        fun setIcon(resourceId: Int): Builder {
            dialog.editableDialogIcon.setImageResource(resourceId)
            return this
        }

        /**
         * Set the icon of the dialog to a drawable.
         *
         * @param icon Drawable.
         */
        fun setIcon(icon: Drawable): Builder {
            dialog.editableDialogIcon.setImageDrawable(icon)
            return this
        }

        /**
         * Set the title of the dialog to a string resource.
         *
         * @param resourceId String resource id.
         */
        fun setTitle(resourceId: Int): Builder {
            dialog.editableDialogTitle.setText(resourceId)
            return this
        }

        /**
         * Set the title of the dialog to a CharSequence.
         *
         * @param title CharSequence.
         */
        fun setTitle(title: CharSequence): Builder {
            dialog.editableDialogTitle.text = title
            return this
        }

        /**
         * Set the message of the dialog to a resource id.
         *
         * @param resourceId String resource id.
         */
        fun setMessage(resourceId: Int): Builder {
            dialog.editableDialogSubtitle.visibility = View.VISIBLE
            dialog.editableDialogSubtitle.setText(resourceId)
            return this
        }

        /**
         * Set the message of the dialog to a CharSequence.
         *
         * @param message CharSequence.
         */
        fun setMessage(message: CharSequence): Builder {
            dialog.editableDialogSubtitle.visibility = View.VISIBLE
            dialog.editableDialogSubtitle.text = message
            return this
        }

        /**
         * Set the text of the dialog Editable to a String resource.
         *
         * @param resourceId String resource id.
         */
        fun setText(resourceId: Int): Builder {
            dialog.editableDialogEditableView.setText(resourceId)
            dialog.editableDialogEditableView.setSelection(dialog.editableDialogEditableView.text.length)
            return this
        }

        /**
         * Set the text of the dialog Editable to a CharSequence.
         *
         * @param text CharSequence.
         */
        fun setText(text: CharSequence): Builder {
            dialog.editableDialogEditableView.setText(text)
            dialog.editableDialogEditableView.setSelection(text.length)
            return this
        }

        /**
         * Set the hint of the dialog Editable to a String resource.
         *
         * @param resourceId String resource id.
         */
        fun setHint(resourceId: Int): Builder {
            dialog.editableDialogEditableView.setHint(resourceId)
            return this
        }

        /**
         * Set the hint of the dialog Editable to a CharSequence.
         *
         * @param hint CharSequence.
         */
        fun setHint(hint: CharSequence): Builder {
            dialog.editableDialogEditableView.hint = hint
            return this
        }

        /**
         * Configure what happens when the user presses OK.
         *
         * @param function A higher-order function.
         */
        fun setPositiveButtonClick(function: (userInput: String) -> Unit): Builder {
            dialog.editableDialogOk.setOnClickListener {
                function(dialog.editableDialogEditableView.text.toString().trim())
                dialog.dismiss()
            }

            return this
        }

        /**
         * Configure what happens when the user presses CANCEL.
         *
         * @param function A higher-order function.
         */
        fun setNegativeButtonClick(function: (userInput: String) -> Unit): Builder {
            dialog.editableDialogCancel.setOnClickListener {
                function(dialog.editableDialogEditableView.text.toString().trim())
                dialog.dismiss()
            }

            return this
        }

    }

}
