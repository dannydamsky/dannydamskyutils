package com.damsky.danny.dannydamskyutils

import android.app.Dialog
import android.content.Context
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import kotlinx.android.synthetic.main.spinner_dialog.*

/**
 * Custom dialog with a Spinner, increase/decrease buttons, a Seekbar and ok/cancel buttons.
 *
 * @param context       Required parameter for any dialog.
 * @param itemList      A list of items that will be used in the spinner.
 * @param spinnerLayout Determines how the TextView of the spinner will look.
 *
 * @author Danny Damsky
 * @since 2018-03-11
 */
class SpinnerDialog private constructor(context: Context, private val itemList: ArrayList<String>,
                                        spinnerLayout: Int = R.layout.simple_spinner_item) : Dialog(context),
        SeekBar.OnSeekBarChangeListener, AdapterView.OnItemSelectedListener {

    init {
        setContentView(R.layout.spinner_dialog)

        spinnerDialogSpinnerView.adapter = ArrayAdapter(context, spinnerLayout, itemList)
        spinnerDialogSpinnerView.onItemSelectedListener = this

        spinnerDialogMinusButton.setOnClickListener {
            if (spinnerDialogSpinnerView.selectedItemPosition == 0)
                spinnerDialogSpinnerView.setSelection(itemList.size - 1)
            else
                spinnerDialogSpinnerView.setSelection(spinnerDialogSpinnerView.selectedItemPosition - 1)
        }

        spinnerDialogPlusButton.setOnClickListener {
            if (spinnerDialogSpinnerView.selectedItemPosition == itemList.size - 1)
                spinnerDialogSpinnerView.setSelection(0)
            else
                spinnerDialogSpinnerView.setSelection(spinnerDialogSpinnerView.selectedItemPosition + 1)
        }

        spinnerDialogSeekbar.max = itemList.size - 1
        spinnerDialogSeekbar.setOnSeekBarChangeListener(this)
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        spinnerDialogSpinnerView.setSelection(progress)
    }


    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        spinnerDialogSeekbar.progress = position
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) = Unit

    override fun onStopTrackingTouch(seekBar: SeekBar?) = Unit

    override fun onNothingSelected(parent: AdapterView<*>?) = Unit

    class Builder(context: Context, private val itemList: ArrayList<String>,
                  spinnerLayout: Int = R.layout.simple_spinner_item) {

        private val dialog = SpinnerDialog(context, itemList, spinnerLayout)

        private fun setDefaultListenersIfNotExist() {
            if (!dialog.spinnerDialogOk.hasOnClickListeners())
                dialog.spinnerDialogOk.setOnClickListener { dialog.dismiss() }

            if (!dialog.spinnerDialogCancel.hasOnClickListeners())
                dialog.spinnerDialogCancel.setOnClickListener { dialog.dismiss() }
        }

        fun show() {
            setDefaultListenersIfNotExist()
            dialog.show()
        }

        fun create(): SpinnerDialog {
            setDefaultListenersIfNotExist()
            return dialog
        }

        /**
         * Set the icon of the dialog to a drawable resource.
         *
         * @param resourceId Drawable resource id.
         */
        fun setIcon(resourceId: Int): Builder {
            dialog.spinnerDialogIcon.setImageResource(resourceId)
            return this
        }

        /**
         * Set the title of the dialog to a string resource.
         *
         * @param resourceId String resource id.
         */
        fun setTitle(resourceId: Int): Builder {
            dialog.spinnerDialogTitle.setText(resourceId)
            return this
        }

        /**
         * Set the title of the dialog to a CharSequence.
         *
         * @param title CharSequence.
         */
        fun setTitle(title: CharSequence): Builder {
            dialog.spinnerDialogTitle.text = title
            return this
        }

        /**
         * Set the message of the dialog to a resource id.
         *
         * @param resourceId String resource id.
         */
        fun setMessage(resourceId: Int): Builder {
            dialog.spinnerDialogSubtitle.visibility = View.VISIBLE
            dialog.spinnerDialogSubtitle.setText(resourceId)
            return this
        }

        /**
         * Set the message of the dialog to a CharSequence.
         *
         * @param message CharSequence.
         */
        fun setMessage(message: CharSequence): Builder {
            dialog.spinnerDialogSubtitle.visibility = View.VISIBLE
            dialog.spinnerDialogSubtitle.text = message
            return this
        }

        /**
         * Configure what happens when the user presses OK.
         *
         * @param function A higher-order function.
         */
        fun setPositiveButtonClick(function: (selectedItem: String, position: Int) -> Unit): Builder {
            dialog.spinnerDialogOk.setOnClickListener {
                function(itemList[dialog.spinnerDialogSpinnerView.selectedItemPosition],
                        dialog.spinnerDialogSpinnerView.selectedItemPosition)
                dialog.dismiss()
            }

            return this
        }

        /**
         * Configure what happens when the user presses CANCEL.
         *
         * @param function A higher-order function.
         */
        fun setNegativeButtonClick(function: (selectedItem: String, position: Int) -> Unit): Builder {
            dialog.spinnerDialogCancel.setOnClickListener {
                function(itemList[dialog.spinnerDialogSpinnerView.selectedItemPosition],
                        dialog.spinnerDialogSpinnerView.selectedItemPosition)
                dialog.dismiss()
            }
            return this
        }
    }
}
