package com.damsky.danny.dannydamskyutilsexample

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.damsky.danny.dannydamskyutils.EditableDialog
import com.damsky.danny.dannydamskyutils.SpinnerDialog
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val arrayList = ArrayList<String>(101)
        for (i in 0 until 101)
            arrayList.add(i.toString())

        val array = arrayOf("abc", "def", "ghi", "jkl", "mno", "pqr", "stu", "vwx", "yz").toList() as ArrayList<String>

        button.setOnClickListener {
            startActivity(Intent(this, DisplayExampleActivity::class.java))
        }

        button2.setOnClickListener {
            startActivity(Intent(this, AboutPageExampleActivity::class.java))
        }

        button3.setOnClickListener {
            SpinnerDialog.Builder(this, arrayList)
                    .setIcon(android.R.drawable.ic_menu_call)
                    .setTitle("This is a title")
                    .setMessage("This is a message")
                    .setPositiveButtonClick { selectedItem, _ ->
                        Toast.makeText(this, "OK has been pressed, selected item was $selectedItem", Toast.LENGTH_SHORT).show()
                    }
                    .setNegativeButtonClick { selectedItem, _ ->
                        Toast.makeText(this, "CANCEL has been pressed, selected item was $selectedItem", Toast.LENGTH_SHORT).show()
                    }
                    .show()
        }

        button4.setOnClickListener {
            EditableDialog.Builder(this, array)
                    .setIcon(android.R.drawable.ic_menu_call)
                    .setTitle("This is a title")
                    .setMessage("This is a message")
                    .setHint("This is a hint")
                    .setText("This is some text")
                    .setPositiveButtonClick { userInput ->
                        Toast.makeText(this, "OK was pressed, text was $userInput", Toast.LENGTH_SHORT).show()
                    }
                    .setNegativeButtonClick { userInput ->
                        Toast.makeText(this, "CANCEL was pressed, text was $userInput", Toast.LENGTH_SHORT).show()
                    }
                    .show()
        }

    }
}
