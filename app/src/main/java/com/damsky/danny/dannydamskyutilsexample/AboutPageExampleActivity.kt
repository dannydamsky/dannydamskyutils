package com.damsky.danny.dannydamskyutilsexample

import android.os.Bundle
import com.damsky.danny.dannydamskyutils.AboutPage
import com.damsky.danny.dannydamskyutils.Display


class AboutPageExampleActivity : AboutPage() {
    private lateinit var display: Display

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        display = Display(this, R.mipmap.ic_launcher)
        setBackButton(true)
    }

    override fun setActivityTheme(): Int {
        return R.style.AppTheme
    }

    override fun setAppIcon(): Int {
        return R.mipmap.ic_launcher
    }

    override fun setAppName(): String {
        return getString(R.string.app_name)
    }

    override fun setVersionIcon(): Int {
        return R.mipmap.ic_launcher
    }

    override fun setVersion(): String {
        return "1.0.0"
    }

    override fun setIntroIcon(): Int {
        return R.mipmap.ic_launcher
    }

    override fun setSourceCodeIcon(): Int {
        return R.mipmap.ic_launcher
    }

    override fun setAuthorTitleIcon(): Int {
        return R.mipmap.ic_launcher
    }

    override fun setAuthorName(): String {
        return "Danny Damsky"
    }

    override fun setAuthorNameIcon(): Int {
        return R.mipmap.ic_launcher
    }

    override fun setAuthorLocation(): String {
        return "Hadera, Israel"
    }

    override fun setAuthorLocationIcon(): Int {
        return R.mipmap.ic_launcher
    }

    override fun setEmailIcon(): Int {
        return R.mipmap.ic_launcher
    }

    override fun setSupportIcon(): Int {
        return R.mipmap.ic_launcher
    }

    override fun setRateIcon(): Int {
        return R.mipmap.ic_launcher
    }

    override fun setDonateIcon(): Int {
        return R.mipmap.ic_launcher
    }

    override fun openIntro() {
        display.showSnackShort("Intro button pressed")
    }

    override fun openSourceCode() {
        display.showSnackShort("Source Code button pressed")
    }

    override fun writeEmail() {
        display.showSnackShort("Email button pressed")
    }

    override fun openStorePageRating() {
        display.showSnackShort("Rate button pressed")
    }

    override fun openStoreDonationPage() {
        display.showSnackShort("Donate button pressed")
    }
}
