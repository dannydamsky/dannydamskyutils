package com.damsky.danny.dannydamskyutilsexample

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.EditText
import android.widget.Spinner
import com.damsky.danny.dannydamskyutils.Display
import kotlinx.android.synthetic.main.activity_display_example.*

class DisplayExampleActivity : AppCompatActivity() {

    companion object {
        const val TITLE = "Title Example"
        const val MESSAGE = "Message Example"
    }

    private lateinit var display: Display
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display_example)
        display = Display(this, R.mipmap.ic_launcher_round)

        showToast.setOnClickListener {
            display.showToastShort(MESSAGE)
        }

        showSnackbar.setOnClickListener {
            display.showSnackShort(MESSAGE)
        }

        showBasicDialog.setOnClickListener {
            display.showBasicDialog(TITLE, MESSAGE, {
                // Do stuff
            })
        }

        showEditText.setOnClickListener {
            val editText = EditText(this)
            display.showEditTextDialog(TITLE, MESSAGE, editText, {
                // Do stuff
            })
        }

        showMultiChoice.setOnClickListener {
            val size = 10
            val list = Array(size) { i: Int -> "$i" }
            val boolList = BooleanArray(10)
            for (i in 0 until 10)
                boolList[i] = false

            display.showMultiChoiceDialog(TITLE, list, boolList, {
                // Do stuff
            })
        }

        showSpinner.setOnClickListener {
            val spinner = Spinner(this)
            val numList = Array(10) { i: Int -> "$i"}.toList()
            display.showSpinnerDialog(TITLE, MESSAGE, spinner, numList, {
                // Do stuff
            })
        }


        showList.setOnClickListener {
            val list = Array(10) {i: Int -> "$i"}
            display.showListDialog(TITLE, list, {
                // Do stuff
            })
        }

        showTimePicker.setOnClickListener {
            display.showTimeDialog(TITLE, 11, 0, { startHour: Int, startMinute: Int ->
                // Do stuff
                display.showSnackShort("Hour: $startHour \t Minute: $startMinute")
            })
        }
    }
}
